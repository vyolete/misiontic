# Estructuras Iteractivas
# While

# Brake

usuario= ""
contraseña= ""
cantidad_intentos= 0
usuario_ingresado=True


while True:
    usuario= input("ingrese su usuario: ")
    contraseña= input("ingrese su contraseña: ")

    if usuario == "lira.antonia" and contraseña == "123" :
        print("Bienvenido")
        usuario_ingresado= False
       
    else:
        cantidad_intentos = cantidad_intentos +1
        print("Usuario o contraseña errónea",cantidad_intentos)
        if cantidad_intentos == 0:
            print(" se cierra por intentos")
            break


#otra forma de hacer el código sin el breake 


"""
usuario= ""
contraseña= ""

usuarioIngresado= True

while usuarioIngresado:
    usuario= input("ingrese su usuario: ")
    contraseña= input("ingrese su contraseña: ")

    if usuario == "lira.antonia" and contraseña == "123" :
        print("Bienvenido")
        usuarioIngresado= False
    else:
        
        cantidad_intentos = cantidad_intentos +1
        print("Usuario o contraseña errónea",cantidad_intentos)


"""


# continue

#Sentecia for

"""
paises= ["colombia","mexico","panama"]

for pais in paises:
    print("hola a toda la poblacion de", pais)

print(" fin de explicacion")

"""

# Ejercicio para crear exponenciales

base= int(input("ingrese la base"))
exponente= int(input("ingrese el exponente"))
total= 1

for exponente in range(1, exponente):
    total = total * base
    print(exponente)
print("Total", total)