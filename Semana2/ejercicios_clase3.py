## Crea un programa que imprima los numerosdel 1 al 100 
#Contiene los que nos divisibles por 11 y qe el programa nos diga
#cuantos son divisibles por 11.

"""
i=0
num_div=0
while i <= 100:
    if (i % 11) == 0:
        num_div=num_div+1
    else :
         print(i)
    i=i+1
print(" La cantidad de numeros divisibles por 11 es: ", num_div)

"""
"""contador= 0
for i in range (0,100):
    if (i+1)%11==0:
        contador+=1
        continue
    print(i+1)
print("numeros divisibles por 11 son:", contador)
"""
# Ejercicio 2

#Buscar el número 23 en la siguiente lista:
#a=[11,34,12,55,3,56,3,32,56,43,77,23,45,67,23,67]
#y saber que tanto tuvo que recorrer hacia adelante y hacia atrás, para ello utilice el método reverse: 

"""
a=[11,34,12,55,3,56,3,32,56,43,77,23,45,67,23,67]
j=0
contador=0
for adelante in a:
    contador +=1
    if adelante == 23:
        print("numero encontrado adelante", contador)


for i in reversed(a):
    j=j+1
    if i == 23 :
        print(" numero encontrado atras")
        print(j)

"""

# Imprmir los enteros del 1 al 20, utilizando un ciclo while y la variable contador i, 
#inicialice la variable i en 0. imprima solamente cinco enteros por linea y separelos
#por tabulacion.

"""
i = 0

while i <= 20:

    print(i+1,"\t",i+2,"\t",i+3,"\t",i+4,"\t",i+5)
    i+=5


"""
## el mismo ejercicio con for
#a=[0,5,10,15]
"""for i in range(0,20,5):
    print(i+1,i+2,i+3,i+4, i+5,sep="\t")
    #print(i+1,"\t",i+2,"\t",i+3,"\t",i+4,"\t",i+5)"""


#######################################################
"""
a= [0,5,10,15]

for i in a:
    linea = ""
    for j in range(0,5):
        linea += str(i+j+1)+"\t"
    print(linea)

"""
#################################################

"""	Calcule el valor de π a partir de la serie 
infinita: 
 
Imprima una tabla que muestre el valor aproximado de π, calculando un término de esta serie, 
dos términos, tres, etcétera. ¿Cuántos términos de 
esta serie tiene que utilizar para obtener 
3.14? ¿3.141? ¿3.1415? ¿3.14159?
"""
"""
pi= 0


iteracionDelFor=2

a=[3.14, 3.141, 3.1415, 3.14159]
for versionpi in a:
    contador = 1
    iteraciones = 1 
    while True:
        if iteraciones % 2 ==1:
         pi+=4/contador
        else:
            pi -= 4/ contador

        if round(pi,iteracionDelFor)==versionpi:
            print(" Pi",pi,"aproximacion", round(pi,iteraciones),"iteraccion",iteraciones,"contador", contador)
            break
        contador+=2
        iteraciones +=1
    iteracionDelFor+=1 """

""" Escriba una aplicación que muestre los 
siguientes patrones por separado, uno debajo del otro. 
Use ciclos for para generar los patrones. 
Todos los asteriscos (*) deben imprimirse 
mediante una sola instrucción print. 
No debe haber ninguna otra instrucción 
de salida en el programa. [Sugerencia: 
los últimos dos patrones requieren que 
cada línea empiece con un número apropiado 
de espacios en blanco]."""

asterisco = "*"
espacio=" "

for i in range(0,10):
    print(asterisco*i)


print("B")

for i in reversed(range(0,11)):
    print(asterisco*i)

print("C")

for i in (range(0,10)):
    print(espacio*(i)+asterisco*(10-i))

print("D")

for i in range(0,10):
    print(espacio*(10-i)+asterisco*(i))
