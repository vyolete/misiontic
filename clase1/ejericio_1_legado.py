

print('\n')
print("************************************************************")
print("    Ejercicio El legado del escudo del Capitán América")
print("************************************************************")

print('\n')

print(""" En una escena eliminada de Falcon and The Winter Soldier, Sam, Bucky y John 
Walker decidieron lanzar el escudo del Capitán América y ver qué tan lejos podría 
llegar si necesitaran atacar de lejos a alguien de los Flag Smashers. 
El escudo de Bucky fue el que más lejos llegó, su lanzamiento -al suprimir cuatro (4) 
metros- superó el doble de metros recorridos por el escudo de Sam. Mientras tanto, 
John Walker se siente frustrado porque la distancia que recorrió su lanzamiento 
es cinco veces menor que la suma de las distancias de Sam y Bucky. 
Adicionalmente, el gobierno de Estados Unidos decidió categorizar estos lanzamientos. 
Si el escudo recorrió entre 0 y 20 metros, está en categoría uno, 
si está entre 21 y 30 metros en categoría dos, entre 31 y 50 metros se cataloga 
como categoría tres, y, por último, la categoría cuatro si es superior 
a los 50 metros. Esto con el fin de poder considerar 
la fuerza de nuestros héroes en el campo de batalla.""")

print('\n')

print(""" Elabore un programa que, dado el lanzamiento de Sam, 
muestre en la primera línea los metros recorridos para los lanzamientos de Sam, 
Bucky y John, separados por un espacio. Luego, en la segunda 
línea imprima en qué categoría quedó el lanzamiento de John Walker.""")

print ('\n')

print(""" Entrada
La entrada es un número entero que representa la distancia en la que 
llegó el escudo lanzado por Sam.
Salida
Tres números enteros separados por un espacio que simbolizan las 
distancias de los lanzamientos de Sam, Bucky y John Walker. 
En la siguiente línea en letras la categoría del lanzamiento de John Walker.""")

print('\n')

# Variable de entrada

sam= int(input (" Ingrese la distancia a la que llegó el escudo lanzado por Sam: "))

bucky= (sam *2)+4 # Distancia de Bucky

John_Walter= (bucky+sam)//5 # Distancia de John_Walter
print('\n')
print( " la distancia de Sam es: ", sam,"\n")
print( " la distancia de Bucky es: ",bucky,"\n")
print("la distancia de John Walker: ", John_Walter,"\n")
print('\n')


if John_Walter >0 and  John_Walter<=20 :
    print(" El escudo de John Walker se encuentra en la categoria uno")
elif John_Walter > 21 and John_Walter <= 30 :
    print(" El escudo de John Walker  está en la categoria dos")
elif John_Walter > 31 and John_Walter <= 50 :
    print("El escudo de John Walker está en la categría tres")
elif John_Walter > 50:
    print ( " El escudo de John Walker está en Categoria superior")

print('*************************************************************************')