# Calcular el salario Bruto

import os
print("\n")
print(" ====== Programa para calular Nomina =====")
print("\n")
nombre= input("Nombre del trabajador: ")
print
horas = int(input("Cantidad de horas trabajadas: "))
valor_por_hora= int (input("Valor por hora: "))
borrarPantalla = lambda: os.system ("clear")
borrarPantalla() #Limpia la pantalla
valor_total= 0
sal_min= 908526

if horas >40:
    valor_total = 40 * valor_por_hora
    valor_total = valor_total + ((horas -40) * (valor_por_hora*1.5))
else :
    valor_total = horas * valor_por_hora
print("\n")
print("===============================================")
print (" las ganancias de:", nombre, " son", valor_total)
print("===============================================")

# si el trabajador gana mas de un millon de pesos debe dejar el 4% de sus ingresos en salud y pension

if valor_total > sal_min :
    deducion= valor_total*0.04
    print(nombre, " Gana mas de un salario minímo","\n")
    print("===============================================")
    print(" sus deduciones son:" , deducion)
    print (" su salario total menos deducciones es:" , valor_total-deducion)
print ("==================================================")