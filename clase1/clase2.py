# Ejercicio 1 clase de fundamentos de programacion mision TIC UPB

# Decalaración de variable


var_str= "Ejercicios de variables y Operaciones" # variable string

var_ent= 18 # variable tipo entera

var_decim= 34.5 # Variable string

var_log = False # variable Booleana

x= 4

y =2

# Operaciones con variables

print('\n' )
print("**************************************")
print(var_str)
print("**************************************")
print('\n' )

print ("la suma de las variables enteras:" , var_ent+var_decim)
print ("la resta de las variables float:" , var_decim-32 )
print ("la division de las variables:" , 25/32 )
print ("la division  parte entera de las variables:" , 25//8) # deben ser numeros enteros 
print ("la exponenciación de las variables:" , 6**3) 
print ("El modulo de la division x y y:", x % y) 
print('\n')

# Operaciones con string
print("**************************************")
print("       Operaciones con Strings")
print("**************************************")
print('\n')
nombre = "lira"
apellido = "Antonia"
nombre_completo= nombre  +" " +apellido # suma cadena de caracteres, se puede separar caracteres con cespacios entre comillas

print(nombre_completo)
print('\n')
nom_tabulado= "Albert \t" 
apellido_tab = "Camus"

print(nom_tabulado+apellido_tab)
print('\n')
print("**************************************")
print("      Operaciones on Booleanos")
print("**************************************")
print('\n')
var_verdadera = True
var_falsa =  False
print('\n')
print(" Ejercucios con Y")
print("**********************")
print('\n')
print(" true y true:" ,True and True) #Verdadero
print(" true y false:" ,True and False) #Falso
print(" false y false:" ,False and False) #Falso
print('\n')
print("Ejercicios con o")
print("**********************")
print('\n')
print(" true y true:" ,True or True) #Verdadero
print(" true y false:" ,True or False) #Falso
print(" false y false:" ,False or False) #Falso
print('\n')

# Operaciones con Booleanos
print("**************************************")
print("     Operaciones con Booleanos")
print("**************************************")
print('\n')

print("¿ Cinco es menor que 2? :")
print()


# Datos de entrada, se utiliza el método input
print('\n')
print("**************************************")
print("     Entrada y salidas")
print("**************************************")
print('\n')

print(" Ingrese su nombre:")
var_nom = input () # Funsion que permite entrada por teclado
var_ape = input (" Ingrese su apellido:")
# Esta entrada se debe cambiar a entero, siempre es modo string




#  Ciclos If - Else

print('\n')
print("**************************************")
print("    Ciclos If - Else")
print("**************************************")
print('\n')

edad = int(input(" Cuántos años tienes?"))

if edad >= 18:
    print(" Eres mayor de edad")
    var_cedula = int(input (" Ingrese su cedula ") )
    print(var_cedula)
else:
    print("Eres menor de edad")
    tarjeta= int(input("¿Cúal es tu tarjeta de identidad?"))
# Este Print no hace parte
print(" Hola" + " " + var_nom +" "+ var_ape)


#  Ciclos Switch case en python

print('\n')
print("**************************************")
print("    Switch case en python")
print("**************************************")
print('\n')

var_opc = int(input("opción"))

def switch_demo(arg):
    switcher = { 
        1: "enero",
        2: "febrero",
        3: "marzo",
        4: "abril",
        5: "mayo",
        6: "junio",
        7: "julio",
    }
    print (switcher.get(arg,"INVALIDO"))


switch_demo(var_opc)

